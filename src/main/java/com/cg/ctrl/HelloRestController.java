package com.cg.ctrl;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@Controller
@RequestMapping("/api")
public class HelloRestController {
	@ResponseBody
	@RequestMapping(value = "/hello")
	public String sayHello() {
		System.out.println("heelo........");
		return "Hello Cg! from Spring Framework!";
	}

	
	@GetMapping(value = "/welcome")
	public String sayWelcome(Model model) {
		String str = "Hello World";
		model.addAttribute("msg", str);
		System.out.println("heelo........");
		return "user";
	}
}
